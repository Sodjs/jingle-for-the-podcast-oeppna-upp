# Jingle for the podcast Öppna upp - statlig öppen källkod

This is the jingle for the podcast Öppna upp (read more about it [here](https://gitlab.com/arbetsformedlingen/www/podcast)).

As a hobby musician as well as a podcaster and an avid supporter of
[free software](https://www.fsf.org/about/what-is-free-software), it seemed natural to choose a music program and music
file which everyone can share, study and modify.

[Listen to the podcast Öppna upp](https://podcasts.google.com/feed/aHR0cHM6Ly9hcmJldHNmb3JtZWRsaW5nZW4uZ2l0bGFiLmlvL3d3dy9wb2RjYXN0L2ZlZWQueG1s?sa=X&ved=0CAMQ4aUDahcKEwjQq_qoy7n6AhUAAAAAHQAAAAAQBQ).


## INSTALL
Install the [Vice emulator](https://vice-emu.sourceforge.io/) and curl, for example:
```
apt-get install vice curl
```

Download and unpack SID-Wizard 1.8 in this directory: https://csdb.dk/release/?id=165302

Install the Vice data files:
```
make install
```

## RUN
Start the DAW like this:
```
make run
```

## DIGITAL AUDIO WORKSTATION
[SID-Wizard 1.8](https://csdb.dk/release/?id=165302) is a powerful and user friendly DAW for the Commodore
64, licensed with [WTFPL](https://en.wikipedia.org/wiki/WTFPL), a permissive free software license, compatible with
the GNU GPL.

Click [here](https://sourceforge.net/p/sid-wizard/code/HEAD/tree/tags/1.8/) for the source code.


## PLAYBACK
![video](media/jingle.0.1.mp4)


## LICENSE
These music files are licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) - please see the
included file LICENSE for details.
