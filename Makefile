install: vice-3.6.1/data SID-Wizard-1.8/application/SID-Wizard-1.8.prg


run: SID-Wizard-1.8/application/SID-Wizard-1.8.prg
	x64 -iecdevice8 -device8 1 -fs8 music-modules \
	-directory vice-3.6.1/data \
	-sidfilters -keyboardmapping 10 -autostartprgmode 1 \
	-poskeymap vice-3.6.1/data/C64/gtk3_sym_se.vkm \
	-symkeymap vice-3.6.1/data/C64/gtk3_sym_se.vkm \
	SID-Wizard-1.8/application/SID-Wizard-1.8.prg


vice-3.6.1/data:
	curl -L https://sourceforge.net/projects/vice-emu/files/releases/vice-3.6.1.tar.gz/download | tar -zxf - vice-3.6.1/data


SID-Wizard-1.8/application/SID-Wizard-1.8.prg:
	@echo "Download an unpack SID-Wizard 1.8 here: https://csdb.dk/release/?id=165302" >&2
	exit 1
